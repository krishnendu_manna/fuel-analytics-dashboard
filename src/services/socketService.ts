
import * as socketIo from 'socket.io-client';
import { EventBus } from '../event-bus/index';
import { config } from '../config'

// const SERVER_URL = 'http://localhost:8000';
const SERVER_URL = config.analyticsWSPrefix;

let socket = null;
export default {
  openSocketRequest(requestId: String): void {
    if (!socket){
      socket = new WebSocket(`${SERVER_URL}?eid=${requestId}`);
      socket.onmessage = (event: any) => {
        EventBus.$emit('messageReceived', event.data);
      }; 
    }
  },

  closeSocket(): void{
    socket.close();
    socket = null;
    console.log('close socket');
  }
};
