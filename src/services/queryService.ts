import { analyticsAxios } from '../utilities/analytics-axios';
import { SocketService } from './index';
import { config } from '../config'
export default {
  analyticsAPIUrl: config.analyticsAPIPrefix,
  async startQuery(queryParams: any): Promise<String> {
    try {
      const response =  await analyticsAxios.post(`${this.analyticsAPIUrl}query`, queryParams);
      if (response && response.data) {
        this.checkStatus(response.data);
        return response.data;
      }
      return undefined;
    } catch (error) {
      console.error(error);
    }
  },

  async checkStatus(requestId: String) {
    try {
      SocketService.openSocketRequest(requestId);
    } catch (error) {
      console.error(error);
    }
  },
  
  async getData(requestId: String, type: String = null ): Promise<any> {
    try {
      let response = null;
      const requestData = {'RequestId': requestId};
      if (type) {
        response =  await analyticsAxios.post(`${this.analyticsAPIUrl}/${type}`, requestData);
      } else {
        response =  await analyticsAxios.get(`${this.analyticsAPIUrl}/${requestId}`);
      }
      if (response && response.data) {
        return response.data;
      }
    } catch (error) {
      console.error(error);
    }
  }
};
