import vue from 'vue';
import vueRouter from 'vue-router';
import allVue from './views/all/all.vue';

vue.use(vueRouter)

export default new vueRouter({
  routes: [
    {
      path: '/all',
      name: 'all',
      component: allVue
    },
    {
      path: '/',
      redirect: '/all'
    }
  ],
  mode: 'abstract',
  linkActiveClass: 'active'
})
