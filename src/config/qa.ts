class Config {
  analyticsAPIPrefix: string;
  analyticsWSPrefix: string;
}

const config: Config = {
  analyticsAPIPrefix: '',
  analyticsWSPrefix: '',
};

export { config }