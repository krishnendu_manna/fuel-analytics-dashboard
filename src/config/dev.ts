class Config {
  analyticsAPIPrefix: string;
  analyticsWSPrefix: string;
}

const config: Config = {
  analyticsAPIPrefix: 'https://api-dev01.mediapipe.com/anadata/v1/',
  analyticsWSPrefix: 'wss://socket-dev01.mediapipe.com/anasocket',
};

export { config }