import { config as devConfig } from './dev'; 
import { config as prodConfig } from './prod';
import { config as qaConfig } from './qa';

let config = devConfig;
if (process.env.NODE_ENV === 'prod') {
  config = prodConfig
} else if (process.env.NODE_ENV === 'test') {
  config = qaConfig
} 
export { config };