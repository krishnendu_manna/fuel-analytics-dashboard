import lodash from 'lodash';

export default {
  /**
   * Convert camelCase to Display Case
   *
   * @param {string} inString
   * @returns {string}
   */
  camelCaseToDisplayCase(inString): String {
    
    if (inString.match(/^[A-Z]*$/)) {
      return inString;
    }  

    let newString = '';
    let charIndex = 0;
    for (let i = 0; i < inString.length; i++) {
      const character = inString[i];
      // If upper and not first character, add a space
      if (character === character.toUpperCase() && charIndex > 0) {
        newString = `${newString} ${character}`;
      } else if (charIndex === 0) {
        // If the first character, make it a capital always
        newString = newString + character.toUpperCase();
      } else {
        newString = newString + character;
      }
      charIndex++;
    }
    return newString;
  },
};
