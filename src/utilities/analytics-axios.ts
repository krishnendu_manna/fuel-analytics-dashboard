// import { config, afterLoadConfig } from "@/config";
import axios from 'axios';
import { ROOTSTORE_TYPES } from '../store/types';
import store from '../store';
const analyticsAxios = axios.create();
analyticsAxios.defaults.baseURL = 'https://localhost:8090/analytics-api/v1';

analyticsAxios.interceptors.request.use(async (config)=>{
  const token = JSON.parse(window.sessionStorage.getItem('oidc.user:https://devshared-sso.mediapipe.com/sso:bitcentral.fuel.webapp')).access_token;
  config.headers.Authorization = `Bearer ${token}`;
  return config;
});

analyticsAxios.interceptors.response.use((response) =>{
  return response;
}, async (error)=>{
  if(error && error.response && (401 === error.response.status)){
    const eventData = {
      name: 'refresh-signin',
      data: window.location.pathname,
    }
    store.commit(ROOTSTORE_TYPES.MUTATIONS.SET_ANALYTICS_EVENT, eventData);
  }else{
    return Promise.reject(error);
  }
})

export { axios, analyticsAxios };
