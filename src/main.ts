// tslint:disable-next-line: import-name
import Vue from 'vue';
import AppVue from './App.vue';
import router from './router';
import store from './store';

// plugins
import vuetify from 'vuetify';

// import 'vuetify/dist/vuetify.min.css';
// import 'material-design-icons-iconfont/dist/material-design-icons.css';

import 'vuelendar/scss/vuelendar.scss';
import 'c3/c3.css';
import './assets/css/_base.scss';

// widget setup
// (optional) 'Custom elements polyfill'
import 'document-register-element/build/document-register-element';
import vueCustomElement from 'vue-custom-element';
Vue.use(vueCustomElement);

Vue.use(vuetify)
Vue.config.productionTip = false;
// use vue-custom-element
AppVue.store = store;
AppVue.router = router;
Vue.customElement('fuel-analytics-dashboard', AppVue);



