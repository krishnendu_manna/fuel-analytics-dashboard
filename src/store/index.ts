import vue from 'vue';
import vuex from 'vuex';
import lodash from 'lodash';
import moment from 'moment';
import { ChartStore } from '@/store/modules/chart';
import { ROOTSTORE_TYPES } from './types';

const { MUTATIONS } = ROOTSTORE_TYPES;
vue.use(vuex)

export const modules = {
  chart: ChartStore,
};

export const state = {
  sectionType: 'all',
  analyticsEvent: {}
};

export const getters = {
  analyticsEvent(state) {
    return state.analyticsEvent;
  },
  sectionType(state) {
    return state.sectionType;
  }
};

export const mutations = {
  // tslint:disable-next-line: function-name
  [MUTATIONS.SET_ANALYTICS_EVENT](state, event) {
    state.analyticsEvent = event;
  },
  // tslint:disable-next-line: function-name
  [MUTATIONS.SET_SECTION_TYPE](state, sectionType) {
    state.sectionType = sectionType;
  }
};

export const actions = {
  
};

export const storeObject = {
  modules,
  state,
  getters,
  mutations,
  actions
};

const store = new vuex.Store(storeObject);

export default store;