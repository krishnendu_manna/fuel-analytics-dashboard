import { Module, MutationTree } from 'vuex';
import lodash from 'lodash';
import { QueryService } from '@/services';

import store from '@/store';
import { CHART_TYPES } from './chart_types';
const { ACTIONS, MUTATIONS } = CHART_TYPES;
export interface ChartState {
  chartData?: any[];
  chartGridData?: any[];
  chartRequestId?: string;
}

// tslint:disable-next-line: variable-name
export const ChartStore: Module<ChartState, any> = {
  namespaced: true,
  state: {
    chartData: [],
    chartGridData: [],
    chartRequestId: null,
  },
  getters: {
    chartData: state => state.chartData,
    chartGridData: state => state.chartGridData,
    chartRequestId: state => state.chartRequestId
  },
  mutations: <MutationTree<ChartState>>{
    // tslint:disable-next-line: function-name
    [MUTATIONS.SET_CHART_DATA](state: ChartState, { payload }) {
      state.chartData = payload;
    },
    // tslint:disable-next-line: function-name
    [MUTATIONS.SET_CHART_GRID_DATA](state: ChartState, { payload }) {
      state.chartGridData = payload;
    },
    // tslint:disable-next-line: function-name
    [MUTATIONS.SET_CHART_REQUEST_ID](state: ChartState, { payload }) {
      state.chartRequestId = payload;
    },
  },
  actions: {
    // tslint:disable-next-line: function-name
    [ACTIONS.START_CHART_QUERY]({ state, commit }) {
      const param = { 'date_range' : { 'start': '2019-09-01 00:00:00',
                                       'end': '2019-09-01 00:20:59'},
                      'granularity': 'minute',
                      'table': 'clipviews',
                      'timezone': 'America/Los_Angeles',
                      'dimensions': [ { 'column': 'eventtime',
                                        'label': 'Y/M/D H:M' } ],
                      'measures': [ { 'column': 'clip.id,sessionid,clipindex', 
                                      'label': 'Clip Views', 
                                      'aggregate': 'count'} ] };
      // const param = {};
      return new Promise((resolve, reject) => {
        QueryService.startQuery(param)
          .then((result) => {
            if (result) {
              commit({
                type: MUTATIONS.SET_CHART_REQUEST_ID,
                payload: result,
              });
              resolve(result);
            } else {
                commit({
                  type: MUTATIONS.SET_CHART_REQUEST_ID,
                  payload: null,
                });
            }
          })
          .catch((e) => {
            reject(false);
          });
      });
    },
    // tslint:disable-next-line: function-name
    [ACTIONS.GET_CHART_DATA]({ state, commit }) {
      return new Promise((resolve, reject) => {
        QueryService.getData(state.chartRequestId)
          .then((result) => {
            if (result) {
              commit({
                type: MUTATIONS.SET_CHART_DATA,
                payload: result,
              });
                commit('updateChartData', result);
              resolve(result);
            } else {
              commit({
                type: MUTATIONS.SET_CHART_DATA,
                payload: [],
              });
            }
          })
          .catch((e) => {
            reject(false);
          });
      });
    },
    // tslint:disable-next-line: function-name
    [ACTIONS.GET_GRID_DATA]({ state, commit }, chartType) {
        return new Promise((resolve, reject) => {
          QueryService.getData(state.chartRequestId, chartType)
            .then((result) => {
              if (result) {
                commit({
                  type: MUTATIONS.SET_CHART_GRID_DATA,
                  payload: result,
                });
                resolve(result);
              } else {
                commit({
                  type: MUTATIONS.SET_CHART_GRID_DATA,
                  payload: [],
                });
              }
            })
            .catch((e) => {
                reject(false);
            });
        });
    },
  }
};

export default ChartStore;
