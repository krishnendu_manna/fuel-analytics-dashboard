const fs = require('fs');
const https = require('https');
const express = require('express');
const port = process.env.PORT || 8081;

const app = express();
app.use(express.static("dist"));
app.get(/.*/, function(req, res) {
  res.sendFile(__dirname + "/dist/index.html");
});

const options = {
  key: fs.readFileSync('certs/development/analytics-selfsigned.key', 'utf8'),
  cert: fs.readFileSync('certs/development/analytics-selfsigned.crt', 'utf8'),
  passphrase: ''
};
const server = https.createServer(options, app)
server.listen(port);

console.log("Server started...");