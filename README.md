# Fuel Analytics Dashboard -  Vue.js embeddable widget
### The command `npm run build` will build your source files for the widget.

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

Add the minified .js and .css file to your application and use the '<fuel-analytics-dashboard></fuel-analytics-dashboard>' tag to embed the dashboard in your webpage.

